{OVERALL_GAME_HEADER}

<!-- 
--------
-- BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
-- Yohoho implementation : Christophe Vallet <christophe.vallet@gmail.com>
-- 
-- This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
-- See http://en.boardgamearena.com/#!doc/Studio for more information.
-------

    yohoho_yohoho.tpl
    
    This is the HTML template of your game.
    
    Everything you are writing in this file will be displayed in the HTML page of your game user interface,
    in the "main game zone" of the screen.
    
    You can use in this template:
    _ variables, with the format {MY_VARIABLE_ELEMENT}.
    _ HTML block, with the BEGIN/END format
    
    See your "view" PHP file to check how to set variables and control blocks
    
    Please REMOVE this comment before publishing your game on BGA
-->


Yohoho ! Amazing game.

<div id="board">
    
    <!-- BEGIN circle -->
    <div id="circle_{N}_{M}" class="circle" style="left: {Xpx}px; top: {Ypx}px;"></div>
    <!-- END circle -->                

	<div id="ships"></div>
	
	<div id="wind"></div>

</div>

<script type="text/javascript">

// Javascript HTML templates

var jstpl_bc='<div class="bc" id="bc_${y_x}"></div>'
var jstpl_bg='<div class="bg" id="bg_${y_x}"></div>'
var jstpl_ba='<div class="ba" id="ba_${y_x}"></div>'
var jstpl_br='<div class="br" id="br_${y_x}"></div>'
var jstpl_wc='<div class="wc" id="wc_${y_x}"></div>'
var jstpl_wg='<div class="wg" id="wg_${y_x}"></div>'
var jstpl_wa='<div class="wa" id="wa_${y_x}"></div>'
var jstpl_wr='<div class="wr" id="wr_${y_x}"></div>'

</script>  

{OVERALL_GAME_FOOTER}
