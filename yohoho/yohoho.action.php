<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Yohoho implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 * 
 * yohoho.action.php
 *
 * Yohoho main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *       
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall( "/yohoho/yohoho/myAction.html", ...)
 *
 */
  
  
  class action_yohoho extends APP_GameAction
  { 
    // Constructor: please do not modify
   	public function __default()
  	{
  	    if( self::isArg( 'notifwindow') )
  	    {
            $this->view = "common_notifwindow";
  	        $this->viewArgs['table'] = self::getArg( "table", AT_posint, true );
  	    }
  	    else
  	    {
            $this->view = "yohoho_yohoho";
            self::trace( "Complete reinitialization of board game" );
      }
  	} 
  	 	
    public function moveShip()
    {
        self::setAjaxMode();     

        // Retrieve arguments
        // Note: these arguments correspond to what has been sent through the javascript "ajaxcall" method
        $yOrigin = self::getArg( "yOrigin", AT_posint, true );
        $xOrigin = self::getArg( "xOrigin", AT_posint, true );
        $yDestination = self::getArg( "yDestination", AT_posint, true );
        $xDestination = self::getArg( "xDestination", AT_posint, true );

        // Then, call the appropriate method in your game logic, like "playCard" or "myAction"
        $this->game->moveShip($yOrigin, $xOrigin, $yDestination, $xDestination );

        self::ajaxResponse( );
    }
    

  }
  

