<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * Yohoho implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * yohoho.view.php
 *
 * This is your "view" file.
 *
 * The method "build_page" below is called each time the game interface is displayed to a player, ie:
 * _ when the game starts
 * _ when a player refreshes the game page (F5)
 *
 * "build_page" method allows you to dynamically modify the HTML generated for the game interface. In
 * particular, you can set here the values of variables elements defined in yohoho_yohoho.tpl (elements
 * like {MY_VARIABLE_ELEMENT}), and insert HTML block elements (also defined in your HTML template file)
 *
 * Note: if the HTML of your game interface is always the same, you don't have to place anything here.
 *
 */
  
  require_once( APP_BASE_PATH."view/common/game.view.php" );
  
  class view_yohoho_yohoho extends game_view
  {
    function getGameName() {
        return "yohoho";
    }
        
  	function build_page( $viewArgs )
  	{		
  	    // Get players & players number
        $players = $this->game->loadPlayersBasicInfos();
        $players_nbr = count( $players );

        /*********** Place your code below:  ************/


        /*
        
        // Examples: set the value of some element defined in your tpl file like this: {MY_VARIABLE_ELEMENT}

        // Display a specific number / string
        $this->tpl['MY_VARIABLE_ELEMENT'] = $number_to_display;

        // Display a string to be translated in all languages: 
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::_("A string to be translated");

        // Display some HTML content of your own:
        $this->tpl['MY_VARIABLE_ELEMENT'] = self::raw( $some_html_code );
        
        */
        
        /*
        
        // Example: display a specific HTML block for each player in this game.
        // (note: the block is defined in your .tpl file like this:
        //      <!-- BEGIN myblock --> 
        //          ... my HTML code ...
        //      <!-- END myblock --> 
        

        $this->page->begin_block( "yohoho_yohoho", "myblock" );
        foreach( $players as $player )
        {
            $this->page->insert_block( "myblock", array( 
                                                    "PLAYER_NAME" => $player['player_name'],
                                                    "SOME_VARIABLE" => $some_value
                                                    ...
                                                     ) );
        }
        
        */        
                
        $this->page->begin_block( "yohoho_yohoho", "circle" );                        
        
        //Hex dimensions
        $hex_marge = 2;
        $hex_width = 64;
        $hex_height = 74;             
		$hex_hor_start = 66;
		$hex_ver_start = 57;
		
		//Circle dimension
		$circle_radius = 30;
        
        //Horizontal distance in px between two circles
        $hor_scale = $hex_width + $hex_marge;
        //Vertical distance in px between two circles
        $ver_scale = round($hex_height/2) + round($hex_width/(2*sqrt(3))) + $hex_marge;
        
        //Position of the first circle
        $x_start = $hex_hor_start + $hex_marge + round($hex_width/2) - $circle_radius;
        $y_start = $hex_ver_start + $hex_marge + round($hex_height/2) - $circle_radius;               
        
        //Let's do the circle rows now, there are 9 rows
        for( $y=1; $y<=9; $y++ )
        {
        	if($y % 2 == 0) {
        		//8 circle for even rows
        		$x_max = 8;
        		//the first circle will start with a shift
        		$x_shift = round($hex_width + $hex_marge)/2;
        	} else {
        		//9 circle for odd rows, and no shift
        		$x_max = 9;   
        		$x_shift = 0;
        	}
        	
        	for( $x=1; $x<=$x_max; $x++ )
        	{
        		$this->page->insert_block("circle", array(
        				'N' => $y,
        				'M' => $x,
        				'Xpx' => round( $x_start + $x_shift + ($x-1)*$hor_scale),
        				'Ypx' => round( $y_start + ($y-1)*$ver_scale)
        		) );
        	}
        }                

        /*********** Do not change anything below this line  ************/
  	}
  }