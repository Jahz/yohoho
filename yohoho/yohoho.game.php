<?php
 /**
  *------
  * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
  * Yohoho implementation : © <Your name here> <Your email address here>
  * 
  * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
  * See http://en.boardgamearena.com/#!doc/Studio for more information.
  * -----
  * 
  * yohoho.game.php
  *
  * This is the main file for your game logic.
  *
  * In this PHP file, you are going to defines the rules of the game.
  *
  */


require_once( APP_GAMEMODULE_PATH.'module/table/table.game.php' );


class Yohoho extends Table
{
	function Yohoho( )
	{
        	
 
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();self::initGameStateLabels( array( 
            //    "my_first_global_variable" => 10,
            //    "my_second_global_variable" => 11,
            //      ...
            //    "my_first_game_variant" => 100,
            //    "my_second_game_variant" => 101,
            //      ...
        ) );
        
	}
	
    protected function getGameName( )
    {
        return "yohoho";
    }	

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame( $players, $options = array() )
    {    
        $sql = "DELETE FROM player WHERE 1 ";
        self::DbQuery( $sql ); 

        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $default_colors = array( "ff0000", "008000");

 
        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = array();
        foreach( $players as $player_id => $player )
        {
            $color = array_shift( $default_colors );
            $values[] = "('".$player_id."','$color','".$player['player_canal']."','".addslashes( $player['player_name'] )."','".addslashes( $player['player_avatar'] )."')";
        }
        $sql .= implode( $values, ',' );
        self::DbQuery( $sql );
        self::reloadPlayersBasicInfos();
        
        /************ Start the game initialization *****/

        // Init global values with their initial values
        //self::setGameStateInitialValue( 'my_first_global_variable', 0 );
        
        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        //self::initStat( 'table', 'table_teststat1', 0 );    // Init a table statistics
        //self::initStat( 'player', 'player_teststat1', 0 );  // Init a player statistics (for all players)
      
        // Init the board
        $sql = "INSERT INTO board (board_y,board_x,board_ship) VALUES ";
        $sql_values = array();
        list( $blackplayer_id, $whiteplayer_id ) = array_keys( $players );
        
        //For each row
        for( $y=1; $y<=9; $y++ ) {
        	
        	//For each column
        	for( $x=1; $x<=9; $x++ ) {
        	
        		$ship_value = "NULL";
        		
        		if($y==1) {
        			if($x == 2 || $x == 3 || $x == 7 || $x == 8) {
        				$ship_value = "'bc'";
        			} else if ($x == 4 || $x == 6) {
        				$ship_value = "'bg'";
        			} else if ($x == 5) {
        				$ship_value = "'br'";
        			} 
        		
        		} else if ($y == 3) {
        			if($x == 5) {
        				$ship_value = "'ba'";
        			}
        		} else if ($y == 7) {
        			if($x == 5) {
        				$ship_value = "'wa'";
        			}
        		
        		} else if ($y == 9) {
        			if($x == 2 || $x == 3 || $x == 7 || $x == 8) {
        				$ship_value = "'wc'";
        			} else if ($x == 4 || $x == 6) {
        				$ship_value = "'wg'";
        			} else if ($x == 5) {
        				$ship_value = "'wr'";
        			}
        		}  

        		$sql_values[] = "('$y','$x',$ship_value)";
        		
        	}
        	
        }        
        
        $sql .= implode( $sql_values, ',' );
        self::DbQuery( $sql );

        // Activate first player (which is in general a good idea :) )
        $this->activeNextPlayer();

        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = array( 'players' => array() );
    
        $current_player_id = self::getCurrentPlayerId();    // !! We must only return informations visible by this player !!
    
        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score FROM player ";
        $result['players'] = self::getCollectionFromDb( $sql );         
        
        // Get yohoho board ships
        $result['board'] = self::getObjectListFromDB( "SELECT board_y y, board_x x, board_ship ship
                                                       FROM board
                                                       WHERE board_ship IS NOT NULL" );
        
        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer beween 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        // TODO: compute and return the game progression

        return 0;
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Utility functions
////////////    

 function getShipAt( $y, $x) {
 	return self::getObjectListFromDB( "SELECT board_ship ship
                                                       FROM board
                                                       WHERE board_y = " + $y + " AND board_x = " + $x ); 	 
 }



//////////////////////////////////////////////////////////////////////////////
//////////// Player actions
//////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in yohoho.action.php)
    */


 function moveShip( $yOrigin, $xOrigin, $yDestination, $xDestination )
    {
        // Check that this player is active and that this action is possible at this moment
        self::checkAction( 'moveShip' );  
        
        $player_id = self::getActivePlayerId(); 
        
        // Now, check if this is a possible move
         $board = self::getObjectListFromDB( "SELECT board_y y, board_x x, board_ship ship
                                                       FROM board
                                                       WHERE board_ship IS NOT NULL" );
        $turnedOverDiscs = self::getTurnedOverDiscs( $x, $y, $player_id, $board );
        
        
        //1 Get the ship we want to move (the one at origin)
        $ship = getShipAt( $yOrigin, $xOrigin);
        
        //2 Check that the ship belongs to the active player
        //TODO        
        
        //3 Check that the ship is allowed to move to its destination
        //TODO
        
        //4 Move the ship
        //4.a Remove the ship from its origin
        $sqlRemoveShip = "UPDATE board SET board_ship = null
        WHERE board_y = " + $yOrigin + " AND board_x = " +  $xOrigin;
        self::DbQuery( $sqlRemoveShip );
        //4.b Add the ship at destination
        $sqlAddShip = "UPDATE board SET board_ship = " + $ship + " 
        WHERE board_y = " + $yDestination + " AND board_x = " +  $xDestination;
        self::DbQuery( $sqlAddShip );
                          
        // Notify
        self::notifyAllPlayers( "moveShip", clienttranslate( '${player_name} moves a ship' ), array(
                'player_id' => $player_id,
                'player_name' => self::getActivePlayerName(),
            ) );

            
        // Then, go to the next state
        $this->gamestate->nextState( 'moveShip' );        
       
    }

    
//////////////////////////////////////////////////////////////////////////////
//////////// Game state arguments
////////////

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */

    /*
    
    Example for game state "MyGameState":
    
    function argMyGameState()
    {
        // Get some values from the current game situation in database...
    
        // return values:
        return array(
            'variable1' => $value1,
            'variable2' => $value2,
            ...
        );
    }    
    */

//////////////////////////////////////////////////////////////////////////////
//////////// Game state actions
////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */
    
    /*
    
    Example for game state "MyGameState":

    function stMyGameState()
    {
        // Do some stuff ...
        
        // (very often) go to another gamestate
        $this->gamestate->nextState( 'some_gamestate_transition' );
    }    
    */

//////////////////////////////////////////////////////////////////////////////
//////////// Zombie
////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
    */

    function zombieTurn( $state, $active_player )
    {
    	$statename = $state['name'];
    	
        if ($state['type'] == "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState( "zombiePass" );
                	break;
            }

            return;
        }

        if ($state['type'] == "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $sql = "
                UPDATE  player
                SET     player_is_multiactive = 0
                WHERE   player_id = $active_player
            ";
            self::DbQuery( $sql );

            $this->gamestate->updateMultiactiveOrNextState( '' );
            return;
        }

        throw new feException( "Zombie mode not supported at this game state: ".$statename );
    }
}
